package oop.lab02.cli;

public class TestCommandLine {

    public static void main(final String[] args) {
        /*
         * Write the main method in such a way that it iterates through the
         * array of arguments, printing each one along with the length of the
         * string - the String class provides a
         * 
         * int length()
         * 
         * method that returns the length of the String.
         * 
         * Example output:


           java oop.lab02.cli.TestCommandLine it's a trap

           The provided arguments are:
            * it's, 4 characters long
            * a, 1 characters long
            * trap, 4 characters long


         */
		 for (String arg : args) {//for each elementi passati (args)
			System.out.println(arg + ", " + arg.length() + " characters long");
		}

    }

}
