package oop.lab02.constructors;

public class Train {

    int nTotSeats;
    int nFCSeats;
    int nSCSeats;

    int nFCReservedSeats;
    int nSCReservedSeats;

   /* void build(final int nTotSeats, final int nFCSeats, final int nSCSeats) {
        this.nTotSeats = nTotSeats;
        this.nFCSeats = nFCSeats;
        this.nSCSeats = nSCSeats;
        this.nFCReservedSeats = 0;
        this.nFCReservedSeats = 0;
    }
*/

	public Train (){
		this (100,300);
	}
	
	public Train(final int nICSeats, final int nIICSeats){
			this.nTotSeats=nICSeats+nIICSeats;
			this.nFCSeats=nICSeats;
			this.nSCSeats=nIICSeats;
			this.nFCReservedSeats = 0;
			this.nFCReservedSeats = 0;
	}
	
		
    void reserveFCSeats(final int nSeats) {
        this.nFCReservedSeats += nSeats;
    }

    void reserveSCSeats(final int nSeats) {
        this.nSCReservedSeats += nSeats;
    }

    double getTotOccupancyRatio() {
        return (this.nFCReservedSeats + this.nSCReservedSeats) * 100d / this.nTotSeats;
    }

    double getFCOccupancyRatio() {
        return this.nFCReservedSeats * 100d / this.nFCSeats;
    }

    double getSCOccupancyRatio() {
        return this.nSCReservedSeats * 100d / this.nSCSeats;
    }

    void deleteAllReservations() {
        this.nFCReservedSeats = 0;
        this.nSCReservedSeats = 0;
    }
	
	void printTrainInfo(){
		System.out.println("ToT seats: "+ this.nTotSeats);
		System.out.println("First Class :"+ this.nFCSeats);
		System.out.println("Second Class: "+ this.nSCSeats);
	}
}
