package oop.lab02.constructors;

public class UseConstructors {

    public static void main(final String[] args) {
        /*
         * 1) Creare lo studente Mario Rossi, matricola 1014, anno
         * immatricolazione 2013
         */
		 Student student=new Student();
		 student.build(01,1014,"Mario","Rossi");
		 student.printStudentInfo();
		 
         /* 2) Creare lo studente Luca Bianchi, matricola 1018, anno
         * immatricolazione 2010
         */ 
         //Student student=new Student();
		 student.build(02,1018,"Luca","Bianchi");
		 student.printStudentInfo();
		 
		 /* 3) Creare lo studente Peppino Vitiello, matricola 1019, anno
         * immatricolazione 2012
         */ 
         //Student student=new Student();
		 student.build(03,1019,"Peppino","Vitiello");
		 student.printStudentInfo();
		 /* 4) Creare lo studente Luca Verdi, matricola 1020, anno
         * immatricolazione 2013
         */ 
         //Student student=new Student();
		 student.build(04,1020,"Luca","Verdi");
		 student.printStudentInfo();
		 /* 5) Creare un treno con 300 posti di cui 100 in prima classe 200 in
         * seconda classe
         * 
         * 6) Creare un treno con 1100 posti di cui 50 in prima classe 1050 in
         * seconda classe
         * 
         * 7) Creare un treno con 500 posti, tutti di seconda classe
         * 
         * 8) Creare un treno con numero di posti di default
         */
         Train[] trains = {//deve essere il metodo build\train
			 new Train(100, 200),
			 new Train(50, 1050),
			 new Train(0, 500),
			 new Train()
		 };
		 
		 
		 /* 9) Per verificare la correttezza dei costruttori implementati
         * stampare a video le informazioni relative agli studenti (metodo
         * printStudentInfo) e ai treni (a tal fine implementare un metodo
         * printTrainInfo nella classe Train). Verificare che il numero di posti
         * di default sia consistente (ossia che ci sia un numero positivo di
         * posti totali, e che la somma dei posti in prima e seconda classe dia
         * il totale dei posti sul treno).
         */
		 
		 for (Train tmp : trains){
			tmp.printTrainInfo();
		 }
		 
    }
}
