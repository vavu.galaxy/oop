package it.unibo.oop.lab03.interfaces;
import it.unibo.oop.lab03.encapsulation.AccountHolder;
import it.unibo.oop.lab03.encapsulation.StrictBankAccount;
import it.unibo.oop.lab03.encapsulation.SimpleBankAccount;
public  class TestBankAccount {

	private static final int DEPOSITO=10000;
	private static final int PRELIEVO=15000;
    private TestBankAccount() { 
	
	};

    public static void main(final String[] args) {
        // 1) Creare l' AccountHolder relativo a Mario Rossi con id 1
         AccountHolder mRossi = new AccountHolder("Mario", "Rossi", 1);
        // 2) Creare l' AccountHolder relativo a Luigi Bianchi con id 2
        final AccountHolder lBianchi = new AccountHolder("Luigi", "Bianchi", 2);
		 
		 /*3) Dichiarare due variabili (acc1 e acc2) di tipo BankAccount */
		 
		 /*4) Creare in acc1 un
         * nuovo oggetto di tipo SimpleBankAccount relativo al conto di Mario Rossi
         * (ammontare iniziale = 0) 5) Creare in acc2 un nuovo oggetto di tipo
         * StrictBankAccount relativo al conto di Luigi Bianchi (ammontare
         * iniziale = 0)*/
		
		final BankAccount acc1 = new SimpleBankAccount(mRossi.getUserID(), 0);
        final BankAccount acc2 = new StrictBankAccount(lBianchi.getUserID(), 0);
		
		 
		 /*
		 6) Prima riflessione: perchè è stato possibile fare la
         * new di due classi diverse in variabili dello stesso tipo? perchè entrambe implementano
			la stessa interfaccia
			7)* Depositare 10000$ in entrambi i conti 8) Prelevare 15000$ in entrambi
         * i conti */
		 
		 acc1.deposit(mRossi.getUserID(),DEPOSITO);
		 acc2.deposit(lBianchi.getUserID(),DEPOSITO);
		 acc1.withdraw(mRossi.getUserID(),PRELIEVO);
		 acc2.withdraw(lBianchi.getUserID(),PRELIEVO );
		 		 /*9) Stampare in stdout l'ammontare corrente 10) Qual'è il
         * risultato e perchè? */
		 System.out.println(mRossi.getName() + " "+ mRossi.getSurname() + "'s account balance is :"
		 + acc1.getBalance());
		 System.out.println(lBianchi.getName() + " "+ lBianchi.getSurname() + "'s account balance is :"
		 + acc2.getBalance());
		 
		/*11) Depositare nuovamente 10000$ in entrambi i
         * conti 11) Invocare il metodo computeManagementFees su entrambi i
         * conti 12) Stampare a video l'ammontare corrente 13) Qual'è il
         * risultato e perchè?
         */
		 acc1.deposit(mRossi.getUserID(), DEPOSITO);
        acc2.deposit(lBianchi.getUserID(), DEPOSITO);
        /*
         * 10) Invocare il metodo computeManagementFees su entrambi i conti
         */
        acc1.computeManagementFees(mRossi.getUserID());
        acc2.computeManagementFees(lBianchi.getUserID());
        /*
         * 11) Stampare a video l'ammontare corrente
         */
        System.out.println(mRossi.getName() + " " + mRossi.getSurname()
            + "'s account balance is " + acc1.getBalance());
        System.out.println(lBianchi.getName() + " " + lBianchi.getSurname()
            + "'s account balance is " + acc2.getBalance());
    }
}
