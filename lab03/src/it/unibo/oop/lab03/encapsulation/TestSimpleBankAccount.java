package it.unibo.oop.lab03.encapsulation;

public final class TestSimpleBankAccount {

    private TestSimpleBankAccount() { }

    public static void main(final String[] args) {
        /*
         * 1) Creare l' AccountHolder relativo a Mario Rossi con id 1 2) Creare
         * l' AccountHolder relativo a Luigi Bianchi con id 2 3) Creare i due
         * SimpleBankAccount corrispondenti 4) Effettuare una serie di depositi e
         * prelievi 5) Stampare a video l'ammontare dei due conti e verificare
         * la correttezza del risultato 6) Provare a prelevare fornendo un idUsr
         * sbagliato 7) Controllare nuovamente l'ammontare
     
	 */
		final AccountHolder mRossi = new AccountHolder("Mario", "Rossi", 1);
		final AccountHolder lBianchi = new AccountHolder("Luigi", "Bianchi", 2);	
		
		final SimpleBankAccount accountRossi = new SimpleBankAccount (mRossi.getUserID(),0);
		final SimpleBankAccount accountBianchi = new SimpleBankAccount (lBianchi.getUserID(),0);
		
		accountRossi.deposit(mRossi.getUserID(), 10000);
        accountRossi.depositFromATM(mRossi.getUserID(), 10000);
        accountBianchi.deposit(lBianchi.getUserID(), 10000);
        accountBianchi.depositFromATM(lBianchi.getUserID(), 10000);
        accountRossi.withdrawFromATM(mRossi.getUserID(), 500);
        accountRossi.withdraw(mRossi.getUserID(), 200);
        accountBianchi.withdrawFromATM(lBianchi.getUserID(), 500);
        accountBianchi.withdrawFromATM(lBianchi.getUserID(), 500);
		
		System.out.println("Account 1 balance is: " + accountRossi.getBalance());
        System.out.println("Account 2 balance is: " + accountBianchi.getBalance());

        // 6) Provare a prelevare fornendo un idUsr sbagliato
        accountRossi.withdraw(7, 340);
        accountBianchi.deposit(8, 900);

        // 7) Controllare nuovamente l'ammontare
        System.out.println("\nAccount 1 balance is: " + accountRossi.getBalance());
        System.out.println("Account 2 balance is: " + accountBianchi.getBalance());

    }
}
