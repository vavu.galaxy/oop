package it.unibo.oop.lab03.encapsulation;
import it.unibo.oop.lab03.interfaces.BankAccount;
public class StrictBankAccount implements BankAccount {

	private static final double ATM_TRANSACTION_FEE = 1;
    private static final double MANAGEMENT_FEE = 5;
    private static final double TRANSACTION_FEE = 0.1;
	
	private final int usrID;
	private double balance;
	private int nTransactions;
	
	public StrictBankAccount ( final int usrID, double balance){ // creazione oggetto
		this.usrID=usrID;
		this.balance=balance;
		this.nTransactions = 0;
	}
	// metodi utili
	private boolean checkUser(final int id){
		return this.usrID == id;
	}
	private void transaction (final int usrID, double amount){
	
		if (checkUser(usrID)){
			this.balance += amount;
			this.incTransaction();
		}
	}	
	private void incTransaction(){
		this.nTransactions++;
	}	
	private boolean isWithdrawAllowed(final double amount) {
        return balance >= amount;
    }
	
	 public void computeManagementFees(final int usrID) {
        final double feeAmount = MANAGEMENT_FEE + (this.nTransactions * TRANSACTION_FEE);
        if (checkUser(usrID) && isWithdrawAllowed(feeAmount)) {
            balance -= feeAmount;
            this.nTransactions = 0;
        }
    }
	
	public void deposit(final int usrID,final double amount) {
        /*
         * Incrementa il numero di transazioni e aggiunge amount al totale del
         * conto Nota: il deposito va a buon fine solo se l'id utente
         * corrisponde
         */
		 
		 this.transaction (usrID,amount);
    }

    public void withdraw(final int usrID,final double amount) {
        /*
         * Incrementa il numero di transazioni e rimuove amount al totale del
         * conto. Note: - Il conto puo' andare in rosso (ammontare negativo) -
         * Il prelievo va a buon fine solo se l'id utente corrisponde
         */
		 if(isWithdrawAllowed(amount)){
			this.transaction (usrID, -amount);
		 }
    }

    public void depositFromATM(final int usrID,final double amount) {
        /*
         * Incrementa il numero di transazioni e aggiunge amount al totale del
         * conto detraendo le spese (costante ATM_TRANSACTION_FEE) relative
         * all'uso dell'ATM (bancomat) Nota: il deposito va a buon fine solo se
         * l'id utente corrisponde
         */
		  this.transaction (usrID, amount - ATM_TRANSACTION_FEE);
    }

    public void withdrawFromATM(final int usrID,final double amount) {
        /*
         * Incrementa il numero di transazioni e rimuove amount + le spese
         * (costante ATM_TRANSACTION_FEE) relative all'uso dell'ATM (bancomat)
         * al totale del conto. Note: - Il conto puo' andare in rosso (ammontare
         * negativo) - Il prelievo va a buon fine solo se l'id utente
         * corrisponde
         */
		if (isWithdrawAllowed(amount)){ 
		  this.transaction (usrID, amount + ATM_TRANSACTION_FEE);
		}	
    }
	
	public double getBalance(){
		return this.balance;
	}

    public int getNTransactions(){
		return this.nTransactions;
	}
	
}