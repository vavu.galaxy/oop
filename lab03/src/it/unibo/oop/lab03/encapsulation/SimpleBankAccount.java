package it.unibo.oop.lab03.encapsulation;
import it.unibo.oop.lab03.interfaces.BankAccount;
public class SimpleBankAccount implements BankAccount {

    private final int usrID;
    /*
     * Aggiungere i seguenti campi:
     * - double balace: ammontare del conto
     * - int userID: id del possessore del conto
     * - int nTransactions: numero delle operazioni effettuate
     * - static double ATM_TRANSACTION_FEE = 1: costo delle operazioni via ATM
     */
	private  double balance;
	private  int nTransactions;
	private static final double ATM_TRANSACTION_FEE=1;
	private static final double MANAGEMENT_FEE = 5;

	
    /*
     * Creare un costruttore pubblico che prenda in ingresso un intero (ossia l'id
     * dell'utente) ed un double (ossia, l'ammontare iniziale del conto corrente).
     */

	 public SimpleBankAccount(final int usrID, final double balance) {
        this.usrID = usrID;
        this.balance = balance;
        this.nTransactions = 0;
    }
	 
    /*
     * Si aggiungano selettori per: 
     * - ottenere l'id utente del possessore del conto
     * - ottenere il numero di transazioni effettuate
     * - ottenere l'ammontare corrente del conto.
     */
	 
	 public int getID(){
		 return this.usrID;
	 }
	 
	 public int getNTransactions(){
		 return this.nTransactions;
	 }
	 
	public double getBalance(){
		return this.balance;
	}	
    
	private void incTransaction(){
		this.nTransactions++;
	}
	
	private void transaction (final int usrID, final double amount){
		if (checkUser(usrID)){
			 this.balance += amount;
			 incTransaction();
		 }
	}
	

		
    public void deposit(final int usrID,final double amount) {
        /*
         * Incrementa il numero di transazioni e aggiunge amount al totale del
         * conto Nota: il deposito va a buon fine solo se l'id utente
         * corrisponde
         */
		 
		 this.transaction (usrID,amount);
    }

    public void withdraw(final int usrID,final double amount) {
        /*
         * Incrementa il numero di transazioni e rimuove amount al totale del
         * conto. Note: - Il conto puo' andare in rosso (ammontare negativo) -
         * Il prelievo va a buon fine solo se l'id utente corrisponde
         */
		 
		 this.transaction (usrID, -amount);
		 
    }

    public void depositFromATM(final int usrID,final double amount) {
        /*
         * Incrementa il numero di transazioni e aggiunge amount al totale del
         * conto detraendo le spese (costante ATM_TRANSACTION_FEE) relative
         * all'uso dell'ATM (bancomat) Nota: il deposito va a buon fine solo se
         * l'id utente corrisponde
         */
		  this.transaction (usrID, amount - SimpleBankAccount.ATM_TRANSACTION_FEE);
    }

    public void withdrawFromATM(final int usrID,final double amount) {
        /*
         * Incrementa il numero di transazioni e rimuove amount + le spese
         * (costante ATM_TRANSACTION_FEE) relative all'uso dell'ATM (bancomat)
         * al totale del conto. Note: - Il conto puo' andare in rosso (ammontare
         * negativo) - Il prelievo va a buon fine solo se l'id utente
         * corrisponde
         */
		  this.transaction (usrID, amount + SimpleBankAccount.ATM_TRANSACTION_FEE);
    }

	public void computeManagementFees(final int usrID) {
        if (checkUser(usrID)) {
            this.balance -= MANAGEMENT_FEE;
        }
    }
	
    /* Utility method per controllare lo user */
    private boolean checkUser(final int id) {
        return this.usrID == id;
    }
}
