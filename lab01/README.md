
# Laboratorio 01

* Si completino gli esercizi in ordine
* All'interno di ogni cartella, è presente un file `README.md` contenente le istruzioni dell'esercizio.
* Si leggano le istruzioni *con molta attenzione*
* Si cerchi di procedere autonomamente, contattando i docenti in caso di necessità
* Al termine di ciascun esercizio, si contattino i docenti per mostrare quanto svolto

**IMPORTANTE**: non si attenda di aver completato più esercizi per chiamare il docente, lo si chiami appena terminato l'esercizio, in modo da evitare di arrivare a fine laboratorio con molti esercizi non corretti. Una volta chiamato il docente, mentre se ne attende l'arrivo, si continui con l'esercizio successivo.
