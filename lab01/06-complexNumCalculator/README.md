# Laboratorio 01

## Svolgimento dell'esercizio

1. Implementare una classe Java `ComplexNumeCalculator` che realizzi una calcolatrice per numeri complessi, in grado di operare sui numeri complessi rappresentati dalla classe `ComplexNum`, con le seguenti caratteristiche:
  * Campi:
    - `int nOpDone`
    - `ComplexNum lastRes`
  * Metodi:
    - `void build()`
    - `ComplexNum add(ComplexNum, ComplexNum)`: addizione
    - `ComplexNum sub(ComplexNum, ComplexNum)`: sottrazione
    - `ComplexNum mul(ComplexNum, ComplexNum)`: moltiplicazione
    - `ComplexNum div(ComplexNum, ComplexNum)`: divisione
2. Si completi la classe `TestComplexNumCalculator` seguendo i commenti in essa contenuti
3. Si compili e si esegua
