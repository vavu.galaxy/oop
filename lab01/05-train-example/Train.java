public class Train{
	int nTotSeats;
	int nFirstClassSeats;
	int nSecondClassSeats;
	int nFirstClassReservedSeats;
	int nSecondClassReservedSeats;
	
	void build (int nFirstClassSeats, int nSecondClassSeats){
		this.nTotSeats=nFirstClassSeats+nSecondClassSeats;
		this.nFirstClassSeats=nFirstClassSeats;
		this.nSecondClassSeats=nSecondClassSeats;
		this.nFirstClassReservedSeats=0;
		this.nSecondClassReservedSeats=0;
	
	}
	
	void reserveFirstClassSeats(int Seats){
		nFirstClassReservedSeats+=Seats;
	}
	
	void reserveSecondClassSeats(int Seats){
		nSecondClassReservedSeats+=Seats;
		
	}
	
	double getTotOccupancyRatio(){
		return ((nFirstClassReservedSeats+nSecondClassReservedSeats)*100)/nTotSeats;
		
	}
	
	double getFirstClassOccupancyRatio(){
		return (nFirstClassReservedSeats*100)/nFirstClassSeats;
	}
	
	double getSecondClassOccupancyRatio(){
		return (nSecondClassReservedSeats*100)/nSecondClassSeats;
		
	}
	
	void deleteAllReservations(){
		
		nFirstClassReservedSeats=0;
		nSecondClassReservedSeats=0;
	}
}