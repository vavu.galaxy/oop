class Calculator{

int nOpDone;
double lastRes;

	void build (){
		this.nOpDone=0;
		this.lastRes=0.0;
	}

	double add(double n1, double n2){
		this.nOpDone++;
		return (this.lastRes=(n1+n2));
	};
	double sub(double n1, double n2){
		this.nOpDone++;
		return (this.lastRes=(n1-n2));
	};
	double mul(double n1, double n2){
		this.nOpDone++;
		return (this.lastRes=(n1*n2));
	};
	double div(double n1, double n2){
		this.nOpDone++;
		return (this.lastRes=(n1/n2));
	};
}