class Student {

    // ... Aggiungere qui la definizione dei campi
	String name;
	String surname;
	int id;
	int matriculationYear;	

    void build(/* Aggiungere i parametri di input */String name,String surname,int id,int matriculationYear) {
        // ... Inizializzazione dei campi della classe
		 this.name=name;
		 this.surname=surname;
		 this.id=id;
		 this.matriculationYear=matriculationYear;
		
    }

    void printStudentInfo() {
        /*
         * Aggiungere i comandi per la stampa delle informazioni sullo studente
         */
		 System.out.println("Nome:"+ this.name +",cognome:"+ this.surname +",id:"+ this.id +",Anno:"+ this.matriculationYear);
    }
}
