class TestStudent {
  public static void main(String[] args) {
      Student student = new Student();
      /*
       * aggiornare l'invocazione del metodo build con l'aggiunta dei
       * parametri
       */
	  Student persona1=new Student();
      persona1.build("matteo","vavusotto",001,2018);
	        persona1.printStudentInfo();
			
	  Student persona3=new Student();
      persona3.build("luca","andreani",032,20234);
	        persona3.printStudentInfo();
	  
	  Student persona2=new Student();
      persona2.build("andrew","saurno",031,2133);
	        persona2.printStudentInfo();

      /*
       * Creare dei nuovi oggetti relativi agli studenti:
       *
       * - Nome: Luigi Cognome: Gentile id: 1015, matriculationYear: 2012 -
       * Nome: Simone Cognome: Bianchi id: 1016, matriculationYear: 2010
       *
       * - Nome: Andrea Cognome: Bracci id: 1017, matriculationYear: 2012
       *
       * Stampare a video le informazioni relative a ciascuno studente.
       */
  }
}
